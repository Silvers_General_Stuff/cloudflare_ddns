import dotenv from 'dotenv'
dotenv.config();

export const email = process.env.email
export const apiKey = process.env.apiKey
export const domain = process.env.domain
export const subDomain = process.env.subDomain
