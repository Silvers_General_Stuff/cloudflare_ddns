import axios from "axios"


// get current IP
export async function getIP (){
    return await axios.get("https://checkip.amazonaws.com/")
        // returns ip if successful
        .then(response => response.data.trim())
        // returns undefined if not
        .catch(_ => undefined)
}

export async function getZoneID (domain:string, email:string, apiKey:string ){
    let response = await axios({
        method:"get",
        url: "https://api.cloudflare.com/client/v4/zones?name="+ domain,
        headers: {
            'X-Auth-Email': email,
            'X-Auth-Key': apiKey,
            "Content-Type": "application/json"
        }
    })
        .then(response => response.data)
        // returns undefined if not
        .catch(_ => { return undefined});

    // pass the failure back
    if(!response){return undefined}
    if(!response.result){return undefined}
    if(!response.result[0]){return undefined}
    return response.result[0].id
}

export function getDDNSDomain(subDomain:string, domain:string){
    let result = "";
    if(subDomain !== ""){
        result+=subDomain;
        result+="."
    }
    result += domain;

    return result
}

export async function checkIfSubdomainExists (zoneID:string, email:string, apiKey:string, dnsDomain:string){
    let response = await axios({
        method:"get",
        url: "https://api.cloudflare.com/client/v4/zones/" + zoneID + "/dns_records?type=A&name="+dnsDomain,
        headers: {
            'X-Auth-Email': email,
            'X-Auth-Key': apiKey,
            "Content-Type": "application/json"
        }
    })
        .then(response => response.data)
        // returns undefined if not
        .catch(_ => { return undefined});

    if(!response){return undefined}
    if(!response.result){return undefined}
    let tmp = {bool:response.result[0], id:undefined, current:undefined};
    if(response.result[0]){
        tmp.id = response.result[0].id;
        tmp.current = response.result[0].content
    }
    return tmp
}

export async function updateDNS (dnsDomain:string, ip:string, zoneID:string, email:string, apiKey:string, entryID:string){
    return await axios({
        method:"put",
        url: "https://api.cloudflare.com/client/v4/zones/" + zoneID + "/dns_records/"+ entryID,
        headers: {
            'X-Auth-Email': email,
            'X-Auth-Key': apiKey,
            "Content-Type": "application/json"
        },
        data:{
            "type":"A",
            "name":dnsDomain,
            "content":ip,
            "ttl":1,
            "proxied":false
        }
    })
        .then(response => response.data)
        // returns undefined if not
        .catch(_ => { return undefined})
}

export async function createDNS (dnsDomain:string, ip:string, zoneID:string, email:string, apiKey:string){
    return await axios({
        method:"post",
        url: "https://api.cloudflare.com/client/v4/zones/" + zoneID + "/dns_records",
        headers: {
            'X-Auth-Email': email,
            'X-Auth-Key': apiKey,
            "Content-Type": "application/json"
        },
        data:{
            "type":"A",
            "name":dnsDomain,
            "content":ip,
            "ttl":1,
            "proxied":false
        }
    })
        .then(response => response.data)
        // returns undefined if not
        .catch(_ => { return undefined})
}