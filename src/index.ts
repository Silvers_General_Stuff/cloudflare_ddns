import {getIP,getZoneID,checkIfSubdomainExists, getDDNSDomain, updateDNS, createDNS} from "./functions"
import {apiKey,domain,email,subDomain} from './config'

main ();
async function main (){
    let dnsDomain = getDDNSDomain(subDomain, domain);

    let ip = await getIP();
    if(!ip){console.log("cannot get IP"); return}

    let zoneID = await getZoneID(domain,email, apiKey);
    if(!zoneID){console.log("cannot get zoneID"); return}

    let exists = await checkIfSubdomainExists(zoneID, email, apiKey, dnsDomain);
    if(typeof exists === "undefined"){console.log("cannot get if domain exists"); return}

    // entry exists
    if(exists.bool){
        // check the current value
        if(exists.current === ip){
            console.log(dnsDomain, "is already", ip, "no need to update" );
            return
        }

        // ip changed, update it
        let result = await updateDNS (dnsDomain, ip, zoneID, email, apiKey, exists.id);
        if(result){
            console.log("Successfully updated",dnsDomain, "with", ip )
        }else{
            console.log("Failed to update",dnsDomain, "with", ip )
        }
    }
    if(!exists.bool){
        // entry does not exist, create it
        let result = await createDNS(dnsDomain,ip,zoneID,email,apiKey);
        if(result){
            console.log("Successfully created",dnsDomain, "with", ip )
        }else{
            console.log("Failed to create",dnsDomain, "with", ip )
        }
    }

}