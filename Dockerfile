FROM node:14 as build_stage
# Create app directory
WORKDIR /app_build
# Install app dependencies
COPY package.json ./
COPY yarn.lock ./
COPY tsconfig.json ./

RUN yarn install
# Copy app source code
COPY . .
# now compile
RUN yarn tsc

# copy to a smaller container
FROM node:14-alpine
COPY --from=build_stage /app_build /
#WORKDIR /app
#Expose port and start application
#EXPOSE 8080
CMD [ "node", "." ]
